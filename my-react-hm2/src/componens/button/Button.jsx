import { Btn } from './styles';
import { Component } from 'react';
import PropTypes from 'prop-types';

class Button extends Component {

  render() {
    const { backgroundColor, text, onClick } = this.props;
    return (
      <Btn onClick={onClick} backgroundColor={backgroundColor}>
        {text}
      </Btn>
    )
  };
}

Button.propTypes = {
  backgroundColor: PropTypes.string,
  text: PropTypes.string,
};

Button.defaultProps = {
  backgroundColor: "black",
  text: "Ok",
};

export default Button;
