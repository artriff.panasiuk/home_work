import { Container } from './styles';
import { Component } from 'react';
import ProductCard from '../card/ProductCard.jsx'
import PropTypes from 'prop-types';
class CardWrapper extends Component {

  render() {
    const { cardsArray, addToCart, addToFavorite } = this.props;
    let cards = null;
    if (Array.isArray(cardsArray)) {
      cards = cardsArray.map((product, ind) => {
        return <ProductCard addToFavoriteFunc={addToFavorite} addToCartFunc={addToCart} key={ind} cardInfo={product} />
      });

    }
    return (
      <Container>
        {cards}
      </Container>
    )
  };
}

CardWrapper.propTypes = {
  cardsArray: PropTypes.array
};

CardWrapper.defaultProps = {
  cardsArray: []
};

export default CardWrapper;