import { Main } from './styles';
import Button from './button/Button';
import Modal from './modal/Modal';
import CardWrapper from './cardWrapper/CardWrapper'
import Header from './header/Header'
import React from 'react';
class App extends React.Component {
  state = {
    modalRender: false,
    productArray: null
  }
  addToCart = (prod) => {
    let arrayCart = localStorage.getItem("ProdInCart");
    if (arrayCart === null) {
      arrayCart = new Array(0);
    } else {
      arrayCart = JSON.parse(arrayCart);
      arrayCart.push(prod); 
    }
    localStorage.setItem("ProdInCart", JSON.stringify(arrayCart));
    this.setState({
      modalRender: true
    })
  }
  addToFavorites = (prod, isFavorite) => {
    let arrayFavorite = JSON.parse(localStorage.getItem("FavoriteProd"));
    if (isFavorite) {
      arrayFavorite.push(prod)
    } else {
      arrayFavorite = arrayFavorite.filter(card => card.productArticle !== prod.productArticle)
    }
    localStorage.setItem("FavoriteProd", JSON.stringify(arrayFavorite));
    this.setState({
      modalRender: false
    })
  }
  closeModal = () => {
    this.setState({
      modalRender: false
    })
  }
  componentDidMount() {
    const url = process.env.PUBLIC_URL + "/productBase.json";
    fetch(url)
      .then(res => res.json())
      .then(data => {
        this.setState({
          productArray: data
        })
      })
  }
  render() {
    let arrayFavorite = localStorage.getItem("FavoriteProd");
    let arrayCart = localStorage.getItem("ProdInCart");
    if (arrayFavorite === null) {
      arrayFavorite = new Array(0);
      localStorage.setItem("FavoriteProd", JSON.stringify(arrayFavorite));
    }
    if (arrayCart === null) {
      arrayCart = new Array(0);
      localStorage.setItem("ProdInCart", JSON.stringify(arrayCart));
    }
    const buttonModal = <Button backgroundColor="rgb(0 0 0 / 28%)" text="Oк" onClick={this.closeModal} />;
    let modalElement = null;
    if (this.state.modalRender) {
      modalElement = <Modal header="Message" text="Product successfully added to your cart!" closeButton={true} onClose={this.closeModal}>{buttonModal}</Modal>;
    }
    const { productArray } = this.state;
    return (
      <>
        {modalElement}
        <Header arrayFavorite={JSON.parse(arrayFavorite)} arrayCart={JSON.parse(arrayCart)} />
        <Main>
          <CardWrapper addToCart={this.addToCart} addToFavorite={this.addToFavorites} cardsArray={productArray} />
        </Main>
      </>
    );
  };
}

export default App;
