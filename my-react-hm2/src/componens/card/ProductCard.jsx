import { CardContainer, CardHeader, ImageContainer, CardDesc, CardFooter } from './styles';
import { Component } from 'react';
import Button from '../button/Button.jsx';
import CheckboxStar from '../checkboxStar/CheckboxStar.jsx';
import PropTypes from 'prop-types';
class ProductCard extends Component {
  state = {
    isFavorite: false
  }
  changeFavorites = () => {
    let value = !this.state.isFavorite;
    const { addToFavoriteFunc, cardInfo } = this.props;
    addToFavoriteFunc(cardInfo, value);
    this.setState({
      isFavorite: value
    })
  }
  render() {
    const { cardInfo, addToCartFunc } = this.props;
    const { productName, price, imgURL, productArticle, productColor } = cardInfo;
    return (
      <CardContainer>
        <ImageContainer>
          <img src={imgURL} alt="card img" />
        </ImageContainer>
        <CardHeader isFavorite={this.state.isFavorite} onClick={this.changeFavorites}>
          <h3 className="card__title">{productName}</h3>
          <CheckboxStar isFavorite={this.state.isFavorite} />
        </CardHeader>
        <CardDesc>
          <p className="card__product-desk">Product color: {productColor}</p>
          <p className="card__product-desk">Article: {productArticle}</p>
        </CardDesc>
        <CardFooter>
          <p className='card__product-price'>{price} &#x24;</p>
          <Button onClick={() => addToCartFunc(cardInfo)} text="add to cart" backgroundColor="rgb(215, 201, 184)" />
        </CardFooter>
      </CardContainer>
    )
  };
}

ProductCard.propTypes = {
  cardInfo: PropTypes.object
};


export default ProductCard;