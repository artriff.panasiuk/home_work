import { Component } from 'react';
import { HeaderSection, Container, Cart, Favorite } from './styled';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart } from '@fortawesome/free-solid-svg-icons';
import { faCartArrowDown } from '@fortawesome/free-solid-svg-icons';
import PropTypes from 'prop-types';

class Header extends Component {

  render() {
    const { arrayFavorite, arrayCart } = this.props;
    return (
      <Container>
        <nav>
          <HeaderSection>Guitar</HeaderSection>
          <div className='icons-container'>
            <Cart>
              <FontAwesomeIcon icon={faCartArrowDown} />
              <span>{arrayCart.length}</span>
            </Cart>
            <Favorite>
              <FontAwesomeIcon icon={faHeart} />
              <span>{arrayFavorite.length}</span>
            </Favorite>
          </div>
        </nav>
      </Container>
    );
  }
}

Header.propTypes = {
  arrayFavorite: PropTypes.array,
  arrayCart: PropTypes.array
};

Header.defaultProps = {
  arrayFavorite: [],
  arrayCart: []
};

export default Header;