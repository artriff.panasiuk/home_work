"use strict";
const container = document.querySelector('.container');
const startBtn = document.querySelector('.start-game');
const selectElement = document.querySelector('.select');
const statusElement = document.querySelector('.result');
let tableElement;
let flags;
let battlefield;
let size = 8;

//Обрати рівень важкості гри
selectElement.addEventListener('change', () => {
  startBtn.disabled = false;
});

const getLevel = () => {
  switch (selectElement.value) {
    case "easy":
      return size = 8;
      break;
    case "middle":
      return size = 16;
      break;
    case "senior":
      return size = 24;
      break;
  }
}

//Cтарт гри
const startGame = (row = size) => {
  let bomb = Math.floor((row * row) / 6);
  flags = bomb;
  //вивести кількість прапорців і мін
  const showStatus = () => {
    statusElement.textContent = `Кількість прапорців ${flags} / Кількість мін ${bomb}`;
  };
  showStatus();

  startBtn.disabled = true;
  //Побудова карти мінувань
  battlefield = new Array(row);
  //Розмітка карти
  for (let i = 0; i < battlefield.length; i++) {
    battlefield[i] = new Array(row);
  }
  for (let i = 0; i < battlefield.length; i++) {
    for (let j = 0; j < battlefield[i].length; j++) {
      battlefield[i][j] = 0;
    }
  }

  //Мінування
  for (let b = 0; b < bomb; b++) {
    let bombY = Math.floor(Math.random() * row);
    let bombX = Math.floor(Math.random() * row);
    if (battlefield[bombY][bombX] == "bomb") {
      b--;
    } else {
      battlefield[bombY][bombX] = "bomb";
    }
  };

  //Індексація зон мінування

  for (let e = 0; e < battlefield.length; e++) {
    for (let d = 0; d < battlefield.length; d++) {
      if (battlefield[e][d] == "bomb") {
        if ((e + 1) < battlefield.length) {
          if (battlefield[e + 1][d] !== "bomb") {
            battlefield[e + 1][d]++; //внизу
          }
        }
        if ((d - 1) >= 0 && (e + 1) < battlefield.length) {
          if (battlefield[e + 1][d - 1] !== "bomb") {
            battlefield[e + 1][d - 1]++; // зліва/вниз
          }
        }
        if ((e - 1) >= 0) {
          if (battlefield[e - 1][d] !== "bomb") {
            battlefield[e - 1][d]++;//вверх
          }

        }
        if ((d + 1) < battlefield.length && (e - 1) >= 0) {
          if (battlefield[e - 1][d + 1] !== "bomb") {
            battlefield[e - 1][d + 1]++; // вверх/вправо
          }
        }
        if ((d + 1) < battlefield.length) {
          if (battlefield[e][d + 1] !== "bomb") {
            battlefield[e][d + 1]++;//вправо
          }
        }
        if ((e + 1) < battlefield.length && (d + 1) < battlefield.length) {
          if (battlefield[e + 1][d + 1] !== "bomb") {
            battlefield[e + 1][d + 1]++; //вправо/вниз
          }
        }
        if ((d - 1) >= 0) {
          if (battlefield[e][d - 1] !== "bomb") {
            battlefield[e][d - 1]++; //вліво
          }
        }
        if ((e - 1) >= 0 && (d - 1) >= 0) {
          if (battlefield[e - 1][d - 1] !== "bomb") {
            battlefield[e - 1][d - 1]++; // вліво/вверх
          }
        }
      }
    }


  };

  //рендерінг карти мінувань
  tableElement = document.createElement("table");
  const classCreator = (arg) => {
    switch (arg) {
      case "bomb":
        return "bomb"
        break;
      case 0:
        return "empty"
        break;
      case 1:
        return "section__one"
        break;
      case 2:
        return "section__two"
        break;
      case 3:
        return "section__three"
        break;
      case 4:
        return "section__four"
        break;
      case 5:
        return "section__five"
        break;
    }
  };
  const renderBattlefield = () => {
    let resultHtml = "";
    for (let i = 0; i < battlefield.length; i++) {
      resultHtml += '<tr>';
      for (let j = 0; j < battlefield.length; j++) {
        resultHtml += `<td><p class = ${classCreator(battlefield[i][j])}>${battlefield[i][j]}</p>`;
      }
    }
    tableElement.insertAdjacentHTML('afterbegin', resultHtml);
    container.append(tableElement);
  };


  renderBattlefield();

  //Event click

  const tdElements = document.querySelectorAll('td');
  const trElements = document.querySelectorAll('tr');
  const pElement = document.querySelectorAll('p');
  const checkResult = () => {
    let countCheckedPlaces = 0;
    for (const td of tdElements) {
      if (td.classList.contains("show")) {
        countCheckedPlaces++;
      }
    }
    if (countCheckedPlaces == (tdElements.length - bomb) && flags == 0) {
      setTimeout(() => {
        alert("Ви перемогли!");
        battlefield = [];
        container.firstElementChild.remove();
        startGame();
      }, 1000);
    }
  };
  const funcLeftClick = (e) => {
    startBtn.disabled = false;
    const endGame = () => {
      tableElement.removeEventListener('click', funcLeftClick);
      tableElement.removeEventListener('contextmenu', funcRightClick);
      for (const cell of pElement) {
        if (cell.classList.contains("bomb")) {
          cell.parentElement.classList.add("show");
        }
      }
      setTimeout(() => alert("Детонація!"), 1000);
    };
    const showNearEmptySpace = (coordinate) => {
      let indexX = coordinate % trElements.length;
      let indexY = Math.floor((coordinate) / trElements.length);
      if (trElements[indexX - 1] !== undefined) {
        trElements[indexY].children[indexX - 1].classList.add("show");
      }

      if (trElements[indexX + 1] !== undefined) {
        trElements[indexY].children[indexX + 1].classList.add("show");
      }

      if (trElements[indexY - 1] !== undefined) {
        trElements[indexY - 1].children[indexX].classList.add("show");
      }

      if (trElements[indexY - 1] !== undefined && trElements[indexY - 1].children[indexX + 1] !== undefined) {
        trElements[indexY - 1].children[indexX + 1].classList.add("show");
      }

      if (trElements[indexY - 1] !== undefined && trElements[indexY - 1].children[indexX - 1] !== undefined) {
        trElements[indexY - 1].children[indexX - 1].classList.add("show");
      }

      if (trElements[indexY + 1] !== undefined) {
        trElements[indexY + 1].children[indexX].classList.add("show");
      }

      if (trElements[indexY + 1] !== undefined && trElements[indexY + 1].children[indexX + 1] !== undefined) {
        trElements[indexY + 1].children[indexX + 1].classList.add("show");
      }

      if (trElements[indexY + 1] !== undefined && trElements[indexY + 1].children[indexX - 1] !== undefined) {
        trElements[indexY + 1].children[indexX - 1].classList.add("show");
      }
    }

    for (let i = 0; i < tdElements.length; i++) {
      if (e.target == tdElements[i] && !tdElements[i].classList.contains("flag")) {
        tdElements[i].classList.add("show");
        if (tdElements[i].firstChild.className == "bomb") {
          endGame();
        }
        if (tdElements[i].firstChild.className == "empty") {
          showNearEmptySpace(i);
        }
      }
    }
    checkResult();
  };
  const funcRightClick = (e) => {
    e.preventDefault();
    for (const td of tdElements) {
      if (e.target == td && !td.classList.contains("show")) {
        if (flags != 0 && !td.classList.contains("flag")) {
          td.classList.toggle("flag");
          flags--;
          showStatus();
        } else if (td.classList.contains("flag")) {
          td.classList.toggle("flag");
          flags++;
          showStatus();
        }
      }
    }
    checkResult();
  };
  tableElement.addEventListener('click', funcLeftClick);
  //Встановити\видалити прапорці
  tableElement.addEventListener('contextmenu', funcRightClick);
};

startGame();

//Кнопка старт нової гри
startBtn.addEventListener('click', () => {
  battlefield = [];
  container.firstElementChild.remove();
  startGame(getLevel());
});





