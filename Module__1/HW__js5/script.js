"use strict";
/*Екранування це техніка запису коду для роботи зі спеціальними символами в рядках, тому що якщо
не екранувати спец.символ то інтерпретатор заборонятиме символ у функціональному контексті
мови програмування.
Засоби оголошення функції в основному бувають двух типів: класична (function nameFunc() { expression...}), і стрілочна (const nameFunc = () + =>{expression...})
expression...
hoisting - це перемінна яка допомагає описати маніпуляції всередені функції над аргументом, який ми будемо передавати.
*/

//Первый вариант

//  function createNewUser() {
//    let newUser = {
//      setFirstName(value) {
//        this._firstName = value;
//      },
//      setLastName(value) {
//       this._lastName = value;
//     },
//     getLogin() {
//       console.log(this._firstName[0].toLowerCase() + this._lastName.toLowerCase());
//     },

//     getAge() {
//       let now = new Date();
//       let age = now.getFullYear() - this._birthday.getFullYear();
//       console.log(age);
//     },

//     getPassword() {
//       console.log(this._firstName[0].toUpperCase() + this._lastName.toLowerCase() + this._birthday.getFullYear());
//     }
//    };
// newUser._firstName = prompt("Enter your first name", "Nick");
// newUser._lastName = prompt("Enter your last name", "Cave");
// newUser._birthday = new Date(prompt("Enter your birthday", "12.12.2010"));
// return newUser;
//  };

// let user = createNewUser();
// console.log(user);
// user.getLogin();
// user.getAge();
// user.getPassword();

//Второй вариант

function createNewUser() {
  let newUser = {
    setFirstName(name) {
      Object.defineProperty(this, 'firstName', {
        value: name
      });
    },
    setLastName(name) {
      Object.defineProperty(this, 'lastName', {
        value: name
      });
    },
    setBirthday(data) {
      Object.defineProperty(this, 'birthday', {
        value: new Date(data)
      });
    },
    getAge() {
      let now = new Date();
      let age = now.getFullYear() - this.birthday.getFullYear();
      console.log(age);
    },
    getLogin() {
      console.log(this.firstName[0].toLowerCase() + this.lastName.toLowerCase());
    },
    getPassword() {
      console.log(this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.getFullYear());
    }
  };
  Object.defineProperty(newUser, 'firstName', {
    value: prompt("Enter your first name", "Nick"),
    writable: false,
    configurable: true
  });
  Object.defineProperty(newUser, 'lastName', {
    value: prompt("Enter your last name", "Cave"),
    writable: false,
    configurable: true
  });
  Object.defineProperty(newUser, 'birthday', {
    value: new Date(prompt("Enter your birthday", "12.12.2010")),
    writable: false,
    configurable: true
  });
  return newUser;
};

let myUser = createNewUser();
myUser.getLogin();
myUser.getAge();
myUser.getPassword();
// myUser.firstName = "TEST1";
// myUser.lastName = "TEST2";
// myUser.birthday = "01.01.2000";
// console.dir(myUser);
myUser.setFirstName("Daniel");
myUser.setLastName("Frie");
myUser.setBirthday("7.01.1978");
console.dir(myUser);
myUser.getLogin();
myUser.getAge();
myUser.getPassword();
