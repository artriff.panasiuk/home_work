"use strict";
let student = {
  tabel: {}
};

Object.defineProperty(student, 'name', {
  value: prompt("Enter student first name","Kolya"),
  writable: false,
  configurable: true
});
Object.defineProperty(student, 'lastName', {
  value: prompt("Enter student last name", "Petrenko"),
  writable: false,
  configurable: true
});
let lessonName = prompt("Enter lesson name","History");
let lessonGrade = +prompt("Enter lesson grade","10");

while (lessonName && lessonGrade) {
  student.tabel[lessonName] = lessonGrade;
  lessonName = prompt("Enter lesson name");
  lessonGrade = +prompt("Enter lesson grade");
}
console.log(student);
(checkBadGrade())?checkAvarageGrade():alert("У Вас есть плохие оценки!!");



function checkBadGrade() {
  for (const key in student.tabel) {
    if (student.tabel[key] < 4) {
       return false;
    };
  };
  alert("Студент переведен на следующий курс.");
  return true;
}

function checkAvarageGrade() {
  let avarageGrade = 0,countLessons = 0;
  for (const key in student.tabel) {
    avarageGrade += student.tabel[key]
    countLessons++;
    };
    avarageGrade /= countLessons;
    if (avarageGrade > 7) {
      alert("Студенту назначена стипендия.");
    } else {
      alert("Студенту неназначена стипендия.");
    }
}