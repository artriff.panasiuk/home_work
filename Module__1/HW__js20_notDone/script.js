"use strict";

const dogBase = [];

const Rex = {
  "data-registration": "22-03-2022",
  "visual_inspaction": {
    "fur": "black",
    "haveWound": false
  },
  "social_info": {
    "name": "Rex",
    "age": "5",
    "breed": "husky",
  },
  "owner": [
    "Jack_Brown",
    "Lisa_Brown"
  ],
};

const Barsic = {
  "data-registration": "12-04-2022",
  "visual_inspaction": {
    "fur": "grey",
    "haveWound": true
  },
  "social_info": {
    "name": "Barsic",
    "age": "13",
    "breed": "corgi",
  },
  "owner": [
    "Gary_Moore",
    "Rita_Moore"
  ]
};

const Barbos = {
  "data-registration": "10-02-2022",
  "visual_inspaction": {
    "fur": "grey",
    "haveWound": true,
    "head": {
      "eye": "blue"
    }
  },
  "social_info": {
    "name": "Barbos",
    "age": "7",
    "breed": "husky",
  },
  "owner": [
    "Jack_Brown",
    "Olyga_Black"
  ]
};

const Lesee = {
  "data-registration": "22-03-2022",
  "visual__inspaction": {
    "fur": "black",
    "haveWound": false
  },
  "social_info": {
    "name": "Lesee",
    "age": "1",
    "breed": "corgi",
  },
  "owner": [
    "Kate_Nicolson",
    "Oleksandr_Moll"
  ]
};

dogBase.push(Rex, Barsic, Barbos, Lesee);

const filterCollection = (arr, keyValue, allMatch, ...keys) => {
  const checkEqualObj = (arr) => {
    let newArr = [];
    newArr = arr.filter((item, index) => arr.indexOf(item) === index);
    return newArr;
  }
  const checkInnerProp = (arr, arrKeys, keyValue) => {
    let result = arr;
    for (let i = 0; i < arrKeys.length; i++) {
      if (result !== undefined) {
        result = result[arrKeys[i]];
      }
    }
    if (typeof result == "object" && !Array.isArray(result)) {
      return Object.keys(result).includes(keyValue);
    } else if (Array.isArray(result)) {
      return result.includes(keyValue);
    } else {
      return result == keyValue;
    }
  };
  let keyValueArr = keyValue.split(" ");
  if (keyValueArr.length > 1) {
    console.log(`Filter by ${keyValue}`);
    let sumArr = [];
    let filteredArr = [];
    if (allMatch) {
      let isTrue = true;
      filteredArr = arr.filter(dog => {
        keys.forEach((key, ind) => {
          // console.log(`${key} ${keyValueArr[ind]}`);
          console.log(`${isTrue}   ${checkInnerProp(dog, key, keyValueArr[ind])} ${dog["social_info"]["name"]}  ${key} ${keyValueArr[ind]}`);
          isTrue = isTrue && checkInnerProp(dog, key, keyValueArr[ind]);
        })
        return isTrue;
      })
      return filteredArr;
    } else {
      keyValueArr.forEach((value, i) => {
        filteredArr = arr.filter(dog => {
          let arrKeys = keys[i].split(".");
          return checkInnerProp(dog, arrKeys, value);

        });
        sumArr = [...sumArr, ...filteredArr];
      });
      return checkEqualObj(sumArr);
    }
  } else {
    console.log(`Filter by ${keyValue}`);
    let key = keys[0];
    return arr.filter(dog => {
      let arrKeys = key.split(".");
      return checkInnerProp(dog, arrKeys, keyValue);

    });
  }
}


console.dir(dogBase);
console.log(filterCollection(dogBase, "22-03-2022", false, "data-registration"));
console.log(filterCollection(dogBase, "grey", false, "visual_inspaction.fur"));
console.log(filterCollection(dogBase, "Rex", false, "social_info.name"));
console.log(filterCollection(dogBase, "corgi", false, "social_info.breed"));
console.log(filterCollection(dogBase, "Kate_Nicolson", false, "owner"));
console.log(filterCollection(dogBase, "eye", false, "visual_inspaction.head"));
console.log(filterCollection(dogBase, "blue", false, "visual_inspaction.head.eye"));

console.log(filterCollection(dogBase, "Barsic grey", false, "social_info.name", "visual_inspaction.fur"));

console.log(filterCollection(dogBase, "corgi black", false, "social_info.breed", "visual_inspaction.fur"));
console.log(filterCollection(dogBase, "corgi black", true, "social_info.breed", "visual_inspaction.fur"));

