"use strict";
const btnMenu = document.querySelector('.btn__menu');
const makeCirclebBtn = document.querySelector('.makeCirclebBtn');
const input = document.querySelector('.input');
const container = document.querySelector('.circles-container');

btnMenu.addEventListener('click',() => {
if (btnMenu.nextElementSibling.classList.contains("show")) {
  btnMenu.nextElementSibling.classList.remove("show");
  btnMenu.nextElementSibling.classList.add("hide");
} else {
  btnMenu.nextElementSibling.classList.add("show");
  btnMenu.nextElementSibling.classList.remove("hide");
}
});

makeCirclebBtn.addEventListener("click", () => {
  if (input.value) {
    for (let i = 0; i < 100; i++) {
      const divElement = document.createElement('div');
      divElement.classList.add("circle");
      divElement.style.width = `${+input.value}px`;
      divElement.style.height = `${+input.value}px`;
      
      const randomColor = Math.floor(Math.random()*16777215).toString(16);
      divElement.style.backgroundColor = `#${randomColor}`;
      container.append(divElement);
    }
    input.value = "";
    input.placeholder = "Диаметр круга";
  
    container.addEventListener('click',(e) => {
      for (const circleElement in container.children) {
      if (container.children[circleElement] == e.target) {
        container.children[circleElement].classList.add("circale-animate");
        setTimeout(() => container.children[circleElement].remove(),1000);
      }
      }
      });
  } else {
    input.placeholder = "Тут пусто!";
  }
});

