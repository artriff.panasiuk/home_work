"use strict";
const keys = document.querySelector('.keys');
const buttons = document.querySelectorAll('.button');
const display = document.querySelector('.display');
let expression = [];
let memory = "";
let flag = false;
let arg = "";
display.firstElementChild.value = "0";
const initalizationArg = (value) => {
  if (arg == "") {
    display.firstElementChild.value = "";
    display.firstElementChild.value += value;
    arg += value;
    console.log(arg);
  } else {
    display.firstElementChild.value += value,
      arg += value;
    console.log(arg);
  }
};
const useMemory = () => {
  if (flag) {
    display.firstElementChild.value = memory;
    expression.push(memory);
    flag = false;
  } else {
    memory = "";
    display.classList.remove("memory-mark");
  }
};
const operationWithMemory = (oper) => {
  display.classList.add("memory-mark");
  flag = true;
  switch (oper) {
    case "-":
      memory -= display.firstElementChild.value;
      break;
    case "+":
      memory += display.firstElementChild.value;
      break;
  }

};
const calc = () => {
  if (expression.length > 1) {
    return eval(expression.join(""));
  }
  return expression[0];
};
const showResult = () => {
  expression.push(arg);
  arg = "";
  display.firstElementChild.value = calc();
  expression = [];
  expression.push(display.firstElementChild.value);
}
keys.addEventListener('click', (e) => {

  buttons.forEach(button => {
    if (button == e.target) {
      switch (button.value) {
        case "=":
          showResult();
          break;
        case "C":
          display.firstElementChild.value = "0";
          expression = [];
          arg = "";
          break;
        case "-":
        case "+":
        case "*":
        case "/":
          showResult();
          expression.push(button.value);
          break;
        case "mrc":
          useMemory();
          break;
        case "m+":
          operationWithMemory("+");
          break;
        case "m-":
          operationWithMemory("-");
          break;
        default:
          initalizationArg(button.value);
          break;
      }
    }
  });
});
document.addEventListener('keydown', (e) => {
  switch (e.key) {
    case "Enter":
      showResult();
      break;
    case "Backspace":
      display.firstElementChild.value = "0";
      expression = [];
      arg = "";
      break;
    case "-":
    case "+":
    case "*":
    case "/":
      showResult();
      expression.push(e.key);
      break;
    case "Shift":
      useMemory();
      break;
    case "Control":
      operationWithMemory("+");
      break;
    case "Alt":
      operationWithMemory("-");
      break;
    case ".":
    case "0":
    case "1":
    case "2":
    case "3":
    case "4":
    case "5":
    case "6":
    case "7":
    case "8":
    case "9":
      initalizationArg(e.key);
      break;
  }
});

