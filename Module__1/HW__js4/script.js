"use strict";
/*Метод объекта - это функция внутри объекта которая может производить определеные операции
 со свойствами объекта.*/
//Первый вариант

//  function createNewUser() {
//    let newUser = {
//      setFirstName(value) {
//        this._firstName = value;
//      },
//      setLastName(value) {
//       this._lastName = value;
//     },
//     getLogin() {
//       console.log(this._firstName[0].toLowerCase() + this._lastName.toLowerCase());
//     }
//    };
// newUser._firstName = prompt("Enter your first name");
// newUser._lastName = prompt("Enter your last name");
// return newUser;
//  };

// let user = createNewUser();
// console.log(user);
// user.getLogin();
// user.setFirstName("Daniel");
// user.setLastName("Frie");
// console.log(user);
// user.getLogin();

//Второй вариант

function createNewUser() {
  let newUser = {
   getLogin() {
     console.log(this.firstName[0].toLowerCase() + this.lastName.toLowerCase());
   },
   setFirstName(name) {
     Object.defineProperty(this, 'firstName', {
       value: name
     });
   },
   setLastName(name) {
     Object.defineProperty(this, 'lastName', {
       value: name
     });
   }
  };
Object.defineProperty(newUser, 'firstName', {
 value: prompt("Enter your first name"),
 writable: false,
 configurable: true
});
Object.defineProperty(newUser, 'lastName', {
 value: prompt("Enter your last name"),
 writable: false,
 configurable: true
});
return newUser;
};

let myUser = createNewUser();
myUser.getLogin();
// myUser.firstName = "TEST1";
// myUser.lastName = "TEST2";
// console.dir(myUser);
myUser.setFirstName("Daniel");
myUser.setLastName("Frie");
console.dir(myUser);
myUser.getLogin();