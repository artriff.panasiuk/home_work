"use strict";
let copyDog = {};
let dog = {
 "visual inspaction": {
  "head":{
    "eye": 2,
    "nose": 1,
    "mouth":{
     "tooth":42,
      "tongue":1
    },
    "ear":2
  },
  "body": {
    "fur":"all body,black",
    "legs": {
    "paw": {
      "claws":4
    },
    "paw": {
      "claws":4
    },
    "paw": {
      "claws":4
    },
    "paw": {
      "claws":4
    }
    }
 
  },
  "tail": 1
 },
 "social info": {
   "name":"Rex",
   "old":"5 years",
   "breed":"husky",
   "owner": {
     "Jack Brown":{
       "tel":"083223233",
       "email":"JB@mail.com"
     },
     "Lisa Brown":{
      "tel":"0838998933",
      "email":"LB@mail.com"
     }
   }
 }
};
copyObject(dog,copyDog);
//Изменяю объект dog, чтоб убедится что это не повлияло на copyDog
dog["social info"].name = "Barsic";
dog["social info"].old = "1 year";
dog["visual inspaction"].body.legs.paw.claws = "none";
dog["social info"].owner["Jack Brown"] = "Harick Gregorian";
delete dog["social info"].owner["Lisa Brown"];
delete dog["visual inspaction"].tail;
//объект copyDog остался без изменений, функция copyObject произвела губокое клонирование
console.dir(copyDog);
console.dir(dog);

function copyObject(originalObj, copyObj) {
  for (const key in originalObj) {
 if (typeof originalObj[key] == "object" && originalObj[key] != "null") {
  copyObj[key] = {};
  copyObject(originalObj[key],copyObj[key]);
 } else {
  copyObj[key] = originalObj[key];
 }
  }
}