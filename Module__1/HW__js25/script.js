"use strict";
document.addEventListener('DOMContentLoaded', function () {
  const prevBtn = document.querySelector('.slider__btn-prev'),
    nextBtn = document.querySelector('.slider__btn-next'),
    cardsField = document.querySelector('.picture__wrapper');
  let positionSlide = 0,
    imgLength = 780,
    cardsLength = document.querySelectorAll('.slider__img').length * imgLength;

  function changePositionSlide (positionSlide) {
   cardsField.style.transform = `translateX(-${positionSlide}px)`;
  };

  nextBtn.addEventListener("click", () => {
     positionSlide += imgLength; 
     if (positionSlide >= cardsLength - imgLength)positionSlide = 0;
    changePositionSlide (positionSlide);
   });

   prevBtn.addEventListener("click", () => {
    (positionSlide > 0)? positionSlide -= 780:positionSlide = cardsLength-imgLength;
      changePositionSlide (positionSlide);
    });
});

