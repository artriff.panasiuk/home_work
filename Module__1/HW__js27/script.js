"use strict";

$('.our-services__menu').click((e) => {
  $('.our-services__tab').each(item => {
    $('.our-services__tab')[item].classList.remove("active-tab");
  })
  showContent(e.target);
  });

  function showContent(tab) {
    $('.our-services__content').each(item => {
      $('.our-services__content')[item].classList.remove("visible-content");
        if($('.our-services__content')[item].dataset.aplienceParts == tab.dataset.aplienceParts) {
          $('.our-services__content')[item].classList.add('visible-content');
        }
        })
      tab.classList.add("active-tab");
    }