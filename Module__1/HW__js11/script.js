"use strict";
const buttons = document.querySelectorAll('.btn');
document.addEventListener('keydown', (e) => {
buttons.forEach((btn) => {
  btn.style.backgroundColor = "black";
  if (e.code === btn.dataset.btn) {
    btn.style.backgroundColor = "blue";
  }
});
});


/*
Не рекомендуется использовать события клавиатуры для input, потому что существуют специальные события input для корректного отслеживания ввода в данное поле.
*/