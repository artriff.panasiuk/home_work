"use strict";
$("document").ready(function () {
  $(".top-rated__wrapper").hide();
$(".section-nav .header-nav__link").click(function(e){
e.preventDefault();
let anchor = $(this).attr('href');  
$('html, body').animate({
scrollTop: $(anchor).offset().top
},800); 
});
$(".btn-top").click(() => {
  $('html, body').animate({
    scrollTop:0
    },800); 
});
$(window).scroll(function(){
if ($(this).scrollTop() > 450) {
$(".btn-top").show("slow");
} else {
$(".btn-top").hide("slow");
}
  });
$(".btn-toggle").click(() => {
  $(".top-rated__wrapper").slideToggle(1000);
});
});