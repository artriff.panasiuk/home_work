"use strict";
// setTimeout() виконує колбек функцію через задану кількість мілісекунд і setInterval()` повторює періодично виконувати колбек функцію через задану кількість мілісекунд.
//Якщо в функцію setTimeout() передати нульову затримку, що код спрацює миттєво.
//Важливо не забувати викликати функцію clearInterval(), щоб зупиняти раніше створений цикл запуску.
const images = document.querySelectorAll('.image-to-show'),
continueBtn = document.querySelector('.btn-continue'),
timer = document.querySelector('.timer'),
stopBtn = document.querySelector('.btn-stop');
let time = 3000,
count = 0,
second= time/1000,
stopSecondTimer, 
imageCount = 0,
stop = [];

function startPresentation(visible="none",count,image = 0) {
 
  for (let i = image; i < images.length; i++) {
    count += time;
  stop.push(setTimeout(hidePicture,count,visible,images[i]));
};
stop.push(setTimeout(() => finish("block",0),count));
};

startPresentation(count); 

stopSecondTimer = setInterval(showSecond,1000);

stopBtn.addEventListener('click', () => {
  fadeAnimate();
  clearInterval(stopSecondTimer);
  continueBtn.disabled = false;
  stopBtn.disabled = true;
  for (const stopID of stop) {
    clearTimeout(stopID);
  }
  stop = [];

  for (let i = 0; i < images.length; i++) {
    if (images[i].style.display == "block") {
      imageCount = i;
      break;
    }
  }
    });

continueBtn.addEventListener('click', () => {
  fadeAnimate();
  stopSecondTimer = setInterval(showSecond,1000);
  startPresentation("none",0,imageCount);
  stopBtn.disabled = false;
  continueBtn.disabled = true;
});

function finish(visible,count) {
  for (const stopID of stop) {
    clearTimeout(stopID);
  }
  stop = [];
  for (const img of images) {
    hidePicture(visible,img);
    startPresentation("none",count);
};
};
function hidePicture(arg,img) {
  img.style.display = arg;
}

function showSecond(){
if(second > 0) {
  timer.innerHTML = second;
  second--;
} else {
  second = 3;
  timer.innerHTML = second;
};
};
function fadeAnimate() {
  for (const image of images) {
    image.classList.toggle("animation-class");
  }
}

/*
Опишите своими словами разницу между функциями setTimeout() и setInterval().
Что произойдет, если в функцию setTimeout() передать нулевую задержку? Сработает ли она мгновенно, и почему?
Почему важно не забывать вызывать функцию clearInterval(), когда ранее созданный цикл запуска вам уже не нужен?
Задание
Реализовать программу, показывающую циклично разные картинки. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

Технические требования:
В папке banners лежит HTML код и папка с картинками.
При запуске программы на экране должна отображаться первая картинка.
Через 3 секунды вместо нее должна быть показана вторая картинка.
Еще через 3 секунды - третья.
Еще через 3 секунды - четвертая.
После того, как покажутся все картинки - этот цикл должен начаться заново.
При запуске программы где-то на экране должна появиться кнопка с надписью Прекратить.
По нажатию на кнопку Прекратить цикл завершается, на экране остается показанной та картинка, которая была там при нажатии кнопки.
Рядом с кнопкой Прекратить должна быть кнопка Возобновить показ, при нажатии которой цикл продолжается с той картинки, которая в данный момент показана на экране.
Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
Необязательное задание продвинутой сложности:
При запуске программы на экране должен быть таймер с секундами и миллисекундами, показывающий сколько осталось до показа следующей картинки.
Делать скрытие картинки и показывание новой картинки постепенным (анимация fadeOut / fadeIn) в течение 0.5 секунды.

подсказка
HW #12. Highlight letters.1). Не управляем src изображений. То есть не нужно менять значение scr для того, чтобы показать нужную картинку. В изначальной верстке присутствуют все изображения. Дальше вы показываете нужные и скрываете ненужные в нужный  момент  времени.  В  данном  случае  можно  использовать  длину коллекции (images.length) и индексы (поочередно менять начиная с индекса 0 и до последнего, после чего возобновить с 0-ля). 2). Если уже нажали на Start, то блокируйте эту кнопку (disabled). И наоборот -если  нажали  уже  на  Stop -то  обратно  активируем  Start  /  Continue  и блокируем уже Stop.  
*/


