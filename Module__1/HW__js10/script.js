"use strict";
const form = document.querySelector('.password-form'),
btnsShowPassword = document.querySelectorAll('.fas'),
warningMessage = document.querySelector('.warning-massage'),
inputPassword = document.querySelector('.input-password'),
inputConfirm = document.querySelector('.input-confirm'),
submitBtn = document.querySelector('.btn');
form.addEventListener('click', (e) => {
  btnsShowPassword.forEach((btn) => {
   if (e.target == btn) {
    changeVisionPassword(btn);
   }
  });
  });
submitBtn.addEventListener("click", (e) => {
e.preventDefault();
if (inputPassword.value == "" || inputConfirm.value == "") {
  alert("Input are empty!!");
} else if(inputPassword.value !== inputConfirm.value) {
  warningMessage.style.visibility = "visible";
} else {
  warningMessage.style.visibility = "hidden";
  alert("You are welcome!");
}
});
  function changeVisionPassword(item) {
    item.classList.toggle("fa-eye-slash");
    (item.previousElementSibling.type == "password")?item.previousElementSibling.type = "text":item.previousElementSibling.type = "password";
  }
