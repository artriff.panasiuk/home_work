"use strict";
let clientNumber;
const returnFactorial = (num) =>{
  return (num != 1)? num * returnFactorial(num - 1): 1;
}

do {
  clientNumber = +prompt("Введите число", 4);
} while (!clientNumber||clientNumber < 0);

alert(returnFactorial(clientNumber));

/*Рекурсия это возможность решить несложную однотипную задачу в качестве самовызывающейся себя функции.*/

