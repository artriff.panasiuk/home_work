"use strict";

/*Метод forEach перебирає кожен елемент масиву,
  та здійснює операцію над нею за допомогою callback функції.
 Якщо потрібно очистити масив,можна через метод spice(0,arr.length), чи можна просто його ссилочну перемінну переназначити на пустий масив(arr = []);
 Перевірити, що та чи інша змінна є масивом, можна за допомогою метода Array.isArray()
  */

function filterBy(arr, filterArg) {
  let resultArray = [];
  arr.forEach(key => {
    switch (filterArg) {
      case "null":
        key === null ? key : resultArray.push(key);
        break;
      case "undefined":
        key === undefined ? key : resultArray.push(key);
        break;
      default: (typeof key === filterArg) && (key != null) ? key : resultArray.push(key);
        break;
    }
  });
  console.log(`Відфільтрований по ${filterArg}: `);
  console.log(resultArray);
};
//FM - filter method
function filterByFM(arr, filterArg) {
  let resultArray = arr.filter(function (key){
    if (filterArg === "null") {
       return key !== null;
    } else if (filterArg === "object") {
      return typeof key !== "object" || key == null;
    } else {
      return typeof key !== filterArg;
    };
  });
  console.log(`Відфільтрований по ${filterArg}: `);
  console.log(resultArray);
};
let array = ['hello', 23, null, true, undefined, [], Symbol()];
console.log(array);
const allTypes = ["string","number","boolean","object","undefined","symbol","null"];
allTypes.forEach(type => {
  // filterBy(array, type);
  filterByFM(array, type);
});

