"use strict";

const btnChangeColorTheme = document.querySelector('.btn-color-theme '),
linkElement = document.createElement("link");
document.head.append(linkElement);
linkElement.rel = "stylesheet";
let flag = localStorage.getItem("OriginColorTheme");

if(flag == null){
  localStorage.setItem("OriginColorTheme", true);
} 
(flag == "false")?linkElement.href = "css/new-color-theme.css":linkElement.href = "";

btnChangeColorTheme.addEventListener('click', () => {
  flag = localStorage.getItem("OriginColorTheme");
  if (flag == "true") {
    linkElement.href = "css/new-color-theme.css";
    localStorage.setItem("OriginColorTheme",false);
  } else {
    linkElement.href = "";
    localStorage.setItem("OriginColorTheme", true);
  }
});

