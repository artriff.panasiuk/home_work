"use strict";
const speedArray = [2,10,3,5];//Массив возможностей обработки таксов за один день различных членов команды
const backlogList = [100,100,100];//количество задач. число - количество затрачених стори поинтов
const deadline = new Date(2022,4,18);
const analizeTaskResolving = (workersCapability,taskList,deadline) => {
  const startTime = new Date();
  let sumSpeed = 0;
  let day = 0;//Это переменная - счётчик дней на выполнения задач при полном задействии потенциала команди
  workersCapability.forEach(workerSpeed => {
    sumSpeed += workerSpeed;
  });
  let daySpeed = sumSpeed;//Это переменная - общий потенциал команды за день
  //Подсчитываем количество необходимых дней в day
  for (let task = 0; task < taskList.length; task++) {
    if (taskList[task] < daySpeed) {
      daySpeed -= taskList[task];
    } else if (taskList[task] == daySpeed){
      day++;
      daySpeed = sumSpeed;
    } else {
      taskList[task] -= daySpeed;
     daySpeed = sumSpeed;
     task--;
     day++;
    }
   }
   let dayWithWeekend = day + Math.floor(day/5)*2;//Добавляем выходные
   startTime.setDate(startTime.getDate() + dayWithWeekend);//Получаем дату выполнения всех задач
   let timeLeft = Math.abs(deadline.getTime() - startTime.getTime());//получаем разницу между дедлайном, в милисекундах
   let dayLeft = Math.ceil(timeLeft/(1000 * 3600 * 24));//Пребразуем в дни
if (startTime < deadline) {//Сравниваем что больше, дедлайн или дата выполнения
  console.log(`Все задачи будут успешно выполнены за ${dayLeft} дней до наступления дедлайна!`);
} else{
  dayLeft -= Math.floor(dayLeft/5)*2;//Убираем выходные
  let extraHours = dayLeft*8;//получаем дополнительные часы на переработку
  console.log(`Команде разработчиков придется потратить допольнительно ${extraHours} часов после дедлайна, чтобы выполнить все задачи в беклоге`);
}
};

analizeTaskResolving(speedArray,backlogList,deadline);










