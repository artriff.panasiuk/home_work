"use strict";
const tableElement = document.createElement("table");
const container = document.querySelector('.container');
const tagMultiplier = (tag, innerTag, amount) => {
  let resultHtml ="";
for (let i = 0; i < amount; i++) {
  resultHtml += tag;
  for (let j = 0; j < amount; j++) {
    resultHtml += innerTag;
  }
}
return resultHtml;
};
tableElement.insertAdjacentHTML('afterbegin',tagMultiplier("<tr>","<td>",30))
container.append(tableElement);
const tdElements = document.querySelectorAll('td'); 
tableElement.addEventListener('click',(e) => {
  for (const td of tdElements) {
    if (e.target == td) {
      td.classList.toggle("black");
    }
  }
});
document.querySelector('body').addEventListener('click', (e) => {
  for (const td of tdElements) {
    if (e.target == document.querySelector('body')) {
     td.classList.toggle("black");
    }
  }
});



