/*
HW8 - searching-dom-tasks
DOM это обєктне представлення елементів сторінки яке створюється браузером з html кода. 
innerHTML можна вставити верстку, в innerText лише текст.
Звернутися до елемента сторінки можна  за допомогою методів getElementById(),getElementsByClassName(),querySelector().
querySelector() кращий спосіб, томущо він дозволяє шукати як по айді так і по классу і по назві селектора.
*/
//Знайти всі параграфи на сторінці та встановити колір фону #ff0000
 const paragraphs = document.querySelectorAll('p');
 for (const p of paragraphs) {
   p.style.backgroundColor = "#ff0000";
 }
//Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
const optionsList = document.getElementById("optionsList");
console.log(optionsList);
console.log(optionsList.parentElement);
for (const childNode of optionsList.childNodes) {
  console.log(`%c Назва ноди ${childNode.nodeName}, тип ноди ${childNode.nodeType} `, 'background: #D23166; color: white padding: 10px 10px;font-size: 14px;border: 1px solid white;');
}
//Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph
document.getElementById('testParagraph').innerText = "This is a paragraph";//на сторінці немає такого классу, є ID 

//Отримати елементи вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
[...document.querySelector('.main-header').children].forEach(child => {
  child.classList.add("nav-item");
  console.log(child);
})
//Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
Array.from(document.querySelectorAll('.options-list-title')).forEach(element => element.classList.remove("options-list-title"));