"use strict";
/*
Завдання HW9 - show-list-on-page 
Новий тег можна створити за допомогою метода createElement("вказати тег") обєкта document, щоб він зявився на сторінці потрібно обрати відносно якого елемента буде відображатись тег, наприклад відносно body, використаєм метод append() - document.body.append("новий тег");

Перший параметр функції insertAdjacentHTML відповідає за орієнтир куди відносно батьківського елемента буде встановленно HTML верстку. afterbegin - після відкриття батьківського тегу, beforebegin - до батьківського тегу, beforend - перед закриттям батьківського тегу, afterend  після  батьківського тегу. 

Щоб видалити елемент зі сторінки, потрібно використати метод remove(), document.document.querySelector('потрібний елемент').remove();
 */
const data = ["Rent", "Dog", "Small",["Nick", "Coca-cola", "Lena",[1,2,3,["second","leg"]]], "Cat", "12", 35 ,"Rocket", "Arm","Water", "Cloth",["Lost","Get","Maid"], "Last", "@"];

const showArray = (arr,itemParent = document.body) => {
const changeArr = (arr) =>  {
  // Функция  changeArr() возвращяет строку с преобразованого массива 
 function transformArr(word){
     /* Функция   transformArr() оборачивает каждый елемент массива в элемент li.Если элемент представляет из себя массив то он добавляет к нему элементы li и ul в качестве первого и последнего элемента в данном массиве  и  переберает рекурсивно продолжает рекурсивно оборачивать элементы в элемент ли, после чего вложенный масив с помощью метода join()  преобразуется в строку */
    if (typeof word == "object") {
      /*создана копия внутреннего массива чтобы избежать изменения изначального массива при добавлении элементов li ul */
      let copyNum = word.slice(); 
      copyNum.unshift("<li><ul>");
      copyNum.push("</li></ul>");
   return copyNum.map( key => (key === "<li><ul>" || key === "</li></ul>")? key:(typeof key == "object")?transformArr(key):key = `<li>${key}</li>`).join('');
    } else {
    return  word = `<li>${word}</li>`;  
    }
  }

  let newArr = arr.map(word => transformArr(word)).join("");
  return newArr
};

itemParent.insertAdjacentHTML("afterbegin", `<div><ul>${changeArr(arr)}</ul><h1>3 second left<h1></div>`);

 function timer() {
   let delay = 1000;
   for (let i = 2; i>=0; i--) {
    setTimeout(() => itemParent.firstChild.children[1].textContent = `${i} second left`, delay);
    delay = delay + 1000;
    i==0?setTimeout(() => itemParent.firstChild.remove(), delay):i;
   };
 }

timer();
};

const el = document.querySelector(".main");
el.style.cssText = "color:white;background-color:grey;margin:0 auto;width:500px;border-radius:10px;padding:10px 20px; font-size:20px";

showArray(data, el);

