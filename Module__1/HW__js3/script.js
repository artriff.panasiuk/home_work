"use strict";
let firstNumber, secondNumber, operationSign;

do {
  firstNumber = +prompt("Введите первое число", 10);
  secondNumber = +prompt("Введите второе число", 10);
  operationSign = prompt("Введите операцию", "+");
} while (!firstNumber || !secondNumber || !operationSign);


console.log(makeCalc(firstNumber,secondNumber,operationSign));

function makeCalc(a, b, mathOperator) {
  switch (mathOperator) {
    case "+":
      return a + b;
    case "-":
      return a - b;
    case "/":
      return a / b;
    case "*":
      return a * b;
    default:
return "Введена неверная операция для вычисления!";
  }
}

// 1.Описать своими словами для чего вообще нужны функции в программировании.
//Ответ: функции помогают создать элемент в коде, который позволяет использовать многократно определенную операцию с различными аргументами.
// 2.Описать своими словами, зачем в функцию передавать аргумент. 
//Ответ:Мы передаем аргументы в функцию для произведения манипуляций  и дальнейшего вывода результата.

