"use strict";
/*Обработчик событий - это функция которая будет работать после того как произойдет определенное событие */ 

const inputElement = document.getElementById("input"),
resultContainer =document.querySelector('.result-container');
inputElement.addEventListener("focusin", ()=> {
  inputElement.style.borderColor = "green";
})
inputElement.addEventListener("focusout", () => {

  if (inputElement.value < 0 || inputElement.value == "" || isNaN(Number(input.value)
  )) {
    changeInputElement("red");
    if (inputElement.nextElementSibling
      == null) {
      let warning = document.createElement("p");
    warning.style.color = "red";
    warning.innerText = "Please enter correct price";
    inputElement.after(warning);
    };
  } else {
    if (inputElement.nextElementSibling
      !== null) {
      inputElement.nextElementSibling
.remove();
    }
    changeInputElement("green","green");
let infoSpan = document.createElement("span"),
btnDelete = document.createElement("button"),
infoContainer = document.createElement("div");
infoContainer.append(infoSpan,btnDelete);
btnDelete.innerText = "X";
infoSpan.innerHTML = `Текущая цена: ${inputElement.value}&#36;`;
resultContainer.append(infoContainer);
btnDelete.addEventListener("click", () => {
  btnDelete.parentElement.remove();
  changeInputElement("transparent","black","");
})
  }
});

function changeInputElement(colorBorder,textColor = "black",value = inputElement.value) {
  inputElement.style.borderColor = colorBorder;
  inputElement.style.color = textColor;
  inputElement.value = value;
}