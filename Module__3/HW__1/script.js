// Теоретичне питання
// Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript?
//Прототипне наслідування використовує сховану властивість об'єкта  - __proto__ , для зберігання необхідних для нас
//методів і ключів конкретно потрібного об'єкта.

// Для чого потрібно викликати super() у конструкторі класу-нащадка?
//super() викликається для використання коструктора батьківського класу

"use script";

class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }

  setName(name) {
    this.name = name;
  }

  setAge(name) {
    this.age = age;
  }

  setSalary(name) {
    this.salary = salary;
  }

  getName() {
    return this.name;
  }

  getAge() {
    return this.age;
  }

  getSalary() {
    return this.salary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }

  setLang() {
    this.lang = lang;
  }

  getLang() {
    return this.lang;
  }

  getSalary() {
    return this.salary * 3;
  }
}


const webDeveloper = new Programmer("Karl", 25, 1000, ["English", "German", "China"]);
console.dir(webDeveloper);

const frontendDeveloper = new Programmer("Jack", 34, 2000, ["English", "German", "China", "Italian"]);
console.dir(frontendDeveloper);

const backendDeveloper = new Programmer("Jane", 18, 170, ["English"]);
console.dir(backendDeveloper);


