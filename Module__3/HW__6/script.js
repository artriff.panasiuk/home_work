// Теоретичне питання
// Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript
//Асихроність - це явище коли виконуються декілька завдань не очікуючи один одного.
//Мова Javascript в виконнані є синхроною, але з за допомогою механізмів вбудованих в браузер, виконується
//розподілення пріортетності виконнаня коду в Stack, спершу виконуються код який не потребує очікування відповіді,
//потім код який потребує очікування.


// Завдання
// Написати програму "Я тебе знайду по IP"
// Технічні вимоги:
// Створити просту HTML-сторінку з кнопкою Знайти по IP.
// Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, отримати звідти IP адресу клієнта.
// Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати інформацію про фізичну адресу.
// під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна /, регіон, місто /, район.
// Усі запити на сервер необхідно виконати за допомогою async await.


"use script";
const API = "https://api.ipify.org/?format=json";
const APIinfo = "http://ip-api.com/json/";
const filter = "?fields=continent,country,regionName,city,district";
const btnSearch = document.querySelector('.header__btn');
const wrapper = document.querySelector('.wrapper');

const sendRequest = (url) => fetch(url).then(response => response.json());

btnSearch.addEventListener('click', async()=>{
  wrapper.innerText = "";
  wrapper.insertAdjacentHTML('afterbegin',`
  <div class="info">
  <h2 class="info__title">Отримана інформація</h2>
  <span class="loader"></span>
  </div>
  `);
  const infoCard = wrapper.querySelector('.info');
  try {
    const ipObj = await sendRequest(API);
    const data = await sendRequest(`${APIinfo}${ipObj.ip}${filter}`);
    infoCard.lastElementChild.remove();
    const {continent,country,regionName,city,district} = data;
    infoCard.insertAdjacentHTML('beforeend',`
    <table>
          <tr>
            <td>Континент</td>
            <td>Країна</td>
            <td>Pегіон</td>
            <td>Місто</td>
            <td>Район</td>
          </tr>
          <tr>
            <td>${continent}</td>
            <td>${country}</td>
            <td>${regionName}</td>
            <td>${city}</td>
            <td>${district}</td>
          </tr>
         </table>
    `);
  } catch (error) {
    infoCard.lastElementChild.remove();
    console.log(error);
  }
})
