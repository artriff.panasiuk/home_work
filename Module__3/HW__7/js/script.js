"use strict";
const rows = document.querySelectorAll('tr');
const table = document.querySelector('table');
const wormCounter = document.querySelector('.worm-point');
const userCounter = document.querySelector('.user-point');
let wormIsAlive;
let wormReborn = true;

class Counter {
  setWormPoint(value) {
    wormCounter.innerText = value;
  }
  setUserPoint(value) { 
    userCounter.innerText = value;
  }
  reset() {
    wormCounter.innerText = 0;
    userCounter.innerText = 0;
  }
}

let counter = new Counter;

class BattleMap {
  setWormPoint() {
    let sumPoint = this.wormPoint + 1;
    if (sumPoint == 50) {
      wormIsAlive = false;
      wormReborn = false;
      counter.setWormPoint(sumPoint);
      createFinalMessage("worm");
    } else {
      this.wormPoint = sumPoint;
      counter.setWormPoint(sumPoint);
    }
  }
  setUserPoint() {
    let sumPoint = this.userPoint + 1;
    if (sumPoint == 50) {
      wormIsAlive = false;
      wormReborn = false;
      counter.setUserPoint(sumPoint);
      createFinalMessage("user");
    } else {
      this.userPoint = sumPoint;
      counter.setUserPoint(sumPoint);
    }
  }
  //Сеттер по побудові пустого поля 10 на 10, з 0 в комірках
  createNewMap() {
    //Побудова карти
    let battlefield = new Array(10);
    //Розмітка карти
    for (let i = 0; i < battlefield.length; i++) {
      battlefield[i] = new Array(10);
    }

    for (let i = 0; i < battlefield.length; i++) {
      for (let j = 0; j < battlefield[i].length; j++) {
        battlefield[i][j] = 0;
        rows[i].children[j].style.background = "rgba(144, 44, 44, 0.615)";
      }
    }
    this.battlefield = battlefield;
    this.wormPoint = 0;
    this.userPoint = 0;
    this.updateBattleMap();
  }
  //геттер значення комірки
  getCellValue(positionY, positionX) {
    return this.battlefield[positionY][positionX];
  }
  //Сеттер значення комірки
  setCellValue(positionY, positionX, value) {
    this.battlefield[positionY][positionX] = value;
    this.updateBattleMap();
  }
  updateBattleMap() {
    for (let i = 0; i < this.battlefield.length; i++) {
      for (let j = 0; j < this.battlefield[i].length; j++) {
        if (this.battlefield[i][j] === 'w') {
          rows[i].children[j].style.backgroundColor = "blue";
        }
        if (this.battlefield[i][j] === 'm') {
          rows[i].children[j].style.backgroundColor = "red";
        }
        if (this.battlefield[i][j] === 'u') {
          rows[i].children[j].style.backgroundColor = "green";
        }
      }
    }
  }
}

let bf = new BattleMap();

class Worm {
  //сеттер координат  черв'яка
  generateLocation(battleMap) {
    let positionY = Math.floor(Math.random() * 10);
    let positionX = Math.floor(Math.random() * 10);
    let cellValue = battleMap.getCellValue(positionY, positionX);
    if (cellValue !== 'w' && cellValue !== 'm' && cellValue !== 'u') {
      battleMap.setCellValue(positionY, positionX, 'w');
      this.location = [positionY, positionX];
    } else {
      this.generateLocation(battleMap);
    }
  }

  setSpeed(value){
    this.levelSpeed = value;
  }

  getLocation() {
    return this.location;
  }

  changePosition(battleMap) {
    setTimeout(() => {
      if (wormIsAlive) {
        battleMap.setWormPoint();
        if (wormIsAlive) {
          battleMap.setCellValue(
            this.getLocation()[0],
            this.getLocation()[1],
            'm'
          );
          this.generateLocation(battleMap);
          this.changePosition(battleMap);
        } else {
          battleMap.setCellValue(
            this.getLocation()[0],
            this.getLocation()[1],
            'm'
          );
        }
      } else if (wormReborn){
        wormIsAlive = true;
        this.generateLocation(battleMap);
        this.changePosition(battleMap);
      }
    }, this.levelSpeed);
  }
}

let worm = new Worm();

class User {
  start(battleMap) {
    table.addEventListener("click", (e) => {
      if (e.target.tagName === "TD") {
        const row = e.target.parentElement;
        const positionX = e.path[0].cellIndex;
        let positionY;
        const body = table.querySelector('tbody').children;
        [...body].forEach((item, index) => {
          if (item === row) {
            positionY = index;
          }
        })
        let cellValue = battleMap.getCellValue(positionY, positionX);
        if (cellValue === 'w') {
          battleMap.setCellValue(positionY, positionX, 'u');
          wormIsAlive = false;
          battleMap.setUserPoint();
        }

      }
    })
  }
}

const user = new User();

function createFinalMessage(message) {
  let mainMessage;
  switch (message) {
    case undefined:
      mainMessage = 'Розпочніть гру';
      break;
    case "worm":
      mainMessage = 'Ви програли!';
      break;
    case "user":
      mainMessage = 'Ви перемогли!';
      break;
  }
  table.insertAdjacentHTML('afterbegin', `
    <div class="message">
    <h1>${mainMessage}</h1>
    <select class="select">
      <option value="1500">Новачок</option>
      <option value="1000">Середній</option>
      <option value="500">Професіонал</option>
    </select>
    <button class="start-game">Почати нову гру</button>
    </div>
    `)
  const select = document.querySelector('.select');
  document.querySelector('.start-game').addEventListener('click', () => {
    const levelSpeed = Number(select.value);
    table.firstElementChild.remove();
    counter.reset();
    bf.createNewMap();
    wormIsAlive = true;
    wormReborn = true;
    user.start(bf);
    worm.setSpeed(levelSpeed);
    worm.generateLocation(bf);
    worm.changePosition(bf);
  })
}

createFinalMessage();

