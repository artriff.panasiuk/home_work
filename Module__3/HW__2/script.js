// Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
//При отриманні некорректних данних від користувача, при отриманні некорретного формату данних з серверу,
//при невірному використанні функції чи методів об'єкта.


// Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript).
// На сторінці повинен знаходитись div з id="root", куди і потрібно буде додати цей список (схоже завдання виконувалось в модулі basic).
// Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність (в об'єкті повинні міститися всі три властивості - author, name, price). Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.
// Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.

"use script";
const rootElement = document.querySelector('#root');
const books = [
  { 
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70 
  }, 
  {
   author: "Сюзанна Кларк",
   name: "Джонатан Стрейндж і м-р Норрелл",
  }, 
  { 
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  }, 
  { 
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  }, 
  {
   author: "Террі Пратчетт",
   name: "Рухомі картинки",
   price: 40
  },
  {
   author: "Анґус Гайленд",
   name: "Коти в мистецтві",
  }
];

renderBooks(books);

function renderBooks(booksArr) {
  function transformInLiElem(){
    return booksArr.map(book => {
    const {name,author,price} = book;
      try {
        if (!name) {
          throw new SyntaxError('Not enough data: name!');
        }
        if (!author) {
          throw new SyntaxError('Not enough data: author!');
        }
        if (!price) {
          throw new SyntaxError('Not enough data: price!');
        }
        return `<li>Книга "${name}", автор ${author}, ціна ${price}</li>`;
      } catch (error) {
        console.log(error.name + ": " + error.message);
      }
    }).join(" ");
  }
  rootElement.insertAdjacentHTML('afterbegin', `<ul>${transformInLiElem()}</ul>`);
}

