"use strict";
const container = document.querySelector('.posts');
const API = "https://ajax.test-danit.com/api/json/";
let cardArray = [];
const titleInput = document.querySelector('.title-input');
const textInput = document.querySelector('.text-input');
const modal = document.querySelector('.add-post__modal');

const sendRequest = async (url, method = 'GET', config = null) => {
  const request = await fetch(url, {
    method: method,
    ...config
  });
  let result = await request;
  if (method === "GET") {
    return result.json();
  } else {
    return result;
  }
}

const getUsers = () => {
  sendRequest(`${API}users`)
    .then(data => {
      data.forEach(({ id, name, email }) => {
        let card = new Card(id, name, email);
        card.createUserCard();
        card.getPost();
        cardArray.push(card);
      });
    })
    .catch((error) => {
      console.log(error.message);
    })
}

class Card {
  constructor(id, name, email) {
    this.id = id;
    this.name = name;
    this.email = email;
  }

  getPost() {
    sendRequest(`${API}posts/${this.id}`)
      .then(post => {
        this.cardElement.lastElementChild.remove();
        this.createUserPost(post)
      })
      .catch((error) => {
        console.log(error.message);
      })

  }

  sendPost(title, body) {
    this.createUserCard();
    const postBody = { title, body };
    sendRequest(`${API}posts/`, 'POST', {
      body: JSON.stringify(postBody),
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(() => {
        this.cardElement.lastElementChild.remove();
        this.createUserPost(postBody);
      })
      .catch((error) => {
        console.log(error.message);
      })

  }

  sendChangedPost(newContent) {
    this.cardElement.querySelector('.change').remove();
    this.cardElement.insertAdjacentHTML('beforeend', `
    <div class="sk-circle">
    <div class="sk-circle1 sk-child"></div>
    <div class="sk-circle2 sk-child"></div>
    <div class="sk-circle3 sk-child"></div>
    <div class="sk-circle4 sk-child"></div>
    <div class="sk-circle5 sk-child"></div>
    <div class="sk-circle6 sk-child"></div>
    <div class="sk-circle7 sk-child"></div>
    <div class="sk-circle8 sk-child"></div>
    <div class="sk-circle9 sk-child"></div>
    <div class="sk-circle10 sk-child"></div>
    <div class="sk-circle11 sk-child"></div>
    <div class="sk-circle12 sk-child"></div>
  </div>
  </div>
    `);
    sendRequest(`${API}posts/${this.id}`, 'PUT', {
      body: JSON.stringify(newContent),
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(() => {
        this.cardElement.lastElementChild.remove();
        this.createUserPost(newContent);
      })
      .catch((error) => {
        console.log(error.message);
      })
  }
 
  createUserPost({ title, body }) {
    this.cardElement.insertAdjacentHTML('beforeend', `
  <div class="post__content">
    <div class="post__title">${title}</div>
    <div class="post__text">${body}</div>
  </div>
  `)
  };

  createUserCard() {
    container.insertAdjacentHTML('afterbegin', `
  <div class="post" id="${this.id}">
  <div class="post__header">
    <div class="post__user">
      <h3 class="post__user-name">${this.name}</h3>
      <a href="mailto:${this.email}" class="post__user-email">${this.email}</a>
    </div>
    <div class="buttons">
      <button class="btn-close">X</button>
      <button class="btn-change">Change</button>
    </div>
  </div>
  <div class="sk-circle">
  <div class="sk-circle1 sk-child"></div>
  <div class="sk-circle2 sk-child"></div>
  <div class="sk-circle3 sk-child"></div>
  <div class="sk-circle4 sk-child"></div>
  <div class="sk-circle5 sk-child"></div>
  <div class="sk-circle6 sk-child"></div>
  <div class="sk-circle7 sk-child"></div>
  <div class="sk-circle8 sk-child"></div>
  <div class="sk-circle9 sk-child"></div>
  <div class="sk-circle10 sk-child"></div>
  <div class="sk-circle11 sk-child"></div>
  <div class="sk-circle12 sk-child"></div>
</div>
</div>
  `)
    this.cardElement = container.querySelector(`.post[id="${this.id}"]`);
  }

  editForm(){
    this.cardElement.lastElementChild.remove();
    this.cardElement.insertAdjacentHTML('beforeend', `
    <div class="post__content change">
      <input class="post__title-input" type="text">
      <textarea class="post__text-input" type="text"></textarea>
    </div>
    `)
  }

  deleteUser() {
    sendRequest(`${API}users/${this.id}`, 'DELETE')
      .then(() => {
        this.removeCardElement();
      })
      .catch((error) => {
        console.log(error.message);
      })
  }

  removeCardElement() {
    this.cardElement.remove();
  }
}

document.querySelector('.add-post').addEventListener('click', (e) => {
  const target = e.target;
  if (target.classList.contains('add-post__btn')) {
    modal.classList.toggle('show');
  }
  if (target.classList.contains('send-btn')) {
    if (titleInput.value && textInput.value) {
      let userCard = new Card(0, 'User', 'User@ukr.net');
      userCard.sendPost(titleInput.value, textInput.value);
      cardArray.push(userCard);
      textInput.value = "";
      titleInput.value = "";
      modal.classList.remove('show');
    } else {
      alert("Write all data!");
    }
  }
});

document.querySelector('.posts').addEventListener('click', (e) => {
  const target = e.target;
  const idElement = +target.closest('.post').id;
  const targetCard = cardArray.find(card => card.id === idElement);
  if (target.classList.contains('btn-close')) {
    targetCard.deleteUser();
  }
  if (target.classList.contains('btn-change')) {
   target.classList.toggle('inchange');
   if (target.classList.contains('inchange')) {
    target.innerText = "Save";
    targetCard.editForm();
   } else {
     const title = document.querySelector('.post__title-input').value;
     const body = document.querySelector('.post__text-input').value;
     target.innerText = "Change";
     if (title && body) {
       const newContent = {title,body};
       targetCard.sendChangedPost(newContent);
     } else {
      alert("Write data!");
      target.classList.toggle('inchange');
      target.innerText = "Save";
     }
   }
  }
});

getUsers(API);



