// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.
//AJAX це технологія запитів яка дозволяє нам отримувати/відправляти інформацію через запити до сервера
// без перезавантаження вебсторінки, тобто в фоновому режимі.

"use strict";

const API = "https://ajax.test-danit.com/api/swapi/films";

const sendRequest = async (url) => {
  const request = await fetch(url);
  const result = await request.json();
  return result;
}

const container = document.querySelector('#films');

const getCharacters = (data) => {
  const films = container.querySelectorAll('.film');
  films.forEach(film => {
    const filmInfo = data.find(info => info.id == film.id);
    const requests = filmInfo.characters.map(url => fetch(url));
    Promise.all(requests)
      .then(responses => {
        const results = responses.map(response => response.json());
        Promise.all(results)
          .then(results => {
            let nameList = results.map(person => `<li>${person.name}</li>`);
            renderCharacters(nameList, film);
          })
          .catch((error) => {
            console.log(error.message);
          })
          .finally(() => {
            const spinner = film.querySelector('.loader');
            spinner.remove();
          })
      });

  });
}

const getFilms = (url) => {
  sendRequest(url)
    .then(data => {
      data.forEach(film => {
        createFilmPost(film);
      });
        getCharacters(data);
    })
    .catch((error) => {
      console.log(error.message);
    })
}

const createFilmPost = ({ episodeId, name, openingCrawl }) => {
  container.insertAdjacentHTML('afterbegin', `
  <div class="film" id="${episodeId}">
  <h2 class="film__name">${name}</h2>
  <div class="film__plot">
  <p class="film__text">${openingCrawl}</p>
  <div class="loader">Loading...</div>
  </div>
  `)
}

const renderCharacters = (arr, parentElement) => {
  parentElement.insertAdjacentHTML('beforeend', `
  <div class = "film__characters">
   <h3>Characters</h3>
   <ul>
   ${arr.join(" ")}
   </ul>
  </div>
  `)
}

getFilms(API);

