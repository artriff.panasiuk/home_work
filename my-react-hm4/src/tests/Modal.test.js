import Modal from './../componens/modal/Modal';
import React from "react";
import { render, unmountComponentAtNode } from "react-dom";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("renders with a title", () => {
  render(<Modal header = "Title"/>,container);
  const titleElement = container.querySelector("h1");
  expect(titleElement.textContent).toBe("Title");
});

it("renders with a text", () => {
  render(<Modal text = "description"/>,container);
  const textElement = container.querySelector("p");
  expect(textElement.textContent).toBe("description");
});


it("renders with a buttons and without any value", () => {
  const btn1 = <button>1</button>;
  const btn2 = <button>2</button>;
  render(<Modal>{btn1}</Modal>,container);
  let buttonWrapper = container.querySelector(".modal").children[2];
  expect(buttonWrapper.children.length).toBe(1);
  render(<Modal>{btn1}{btn2}</Modal>,container);
   buttonWrapper = container.querySelector(".modal").children[2];
  expect(buttonWrapper.children.length).toBe(2);
  render(<Modal/>,container);
   buttonWrapper = container.querySelector(".modal").children[2];
  expect(buttonWrapper.children.length).toBe(0);
});


