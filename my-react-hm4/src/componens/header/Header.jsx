import { HeaderSection, Container, Cart, Favorite } from './styled';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart } from '@fortawesome/free-solid-svg-icons';
import { faCartArrowDown } from '@fortawesome/free-solid-svg-icons';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import {selectCartArray, selectFavoriteProd} from '../../store/selectors';


const Header = () => {
  const favoriteProd = useSelector(selectFavoriteProd);
  const arrayCart = useSelector(selectCartArray);
  return (
    <Container>
      <nav>
        <NavLink to="/"
          style={{ textDecoration: "none" }}>
          <HeaderSection>Guitar</HeaderSection>
        </NavLink>
        <div className='icons-container'>
          <NavLink to="/cart">
            <Cart>
              <FontAwesomeIcon icon={faCartArrowDown} />
              <span>{arrayCart.length}</span>
            </Cart>
          </NavLink>
          <NavLink to="/favorite">
            <Favorite>
              <FontAwesomeIcon icon={faHeart} />
              <span>{favoriteProd.length}</span>
            </Favorite>
          </NavLink>
        </div>
      </nav>
    </Container>
  );
}

Header.propTypes = {
  arrayCart: PropTypes.array
};

Header.defaultProps = {
  arrayCart: []
};

export default Header;