import { Container } from './styles';
import ProductCard from '../card/ProductCard.jsx'
import { useSelector } from 'react-redux';
import {selectFavoriteProd} from '../../store/selectors';
const FavoriteCardWrapper = () => {
  const favoriteProd = useSelector(selectFavoriteProd);
  return (
    <Container>
      {
       favoriteProd && favoriteProd.map((product, ind) => {
          return <ProductCard key={ind} cardInfo={product} isFavoriteProd={true} isInFavorite={true}/>
        })
      }
    </Container>
  )
}


export default FavoriteCardWrapper;