import { Container } from './styles';
import ProductCard from '../card/ProductCard.jsx'
import { useSelector } from 'react-redux';
import {selectCartArray} from '../../store/selectors';

const CartWrapper = () => {
  const arrayCart = useSelector(selectCartArray);

    return (
      <Container>
        {
           arrayCart.map((product, ind) => {
            return <ProductCard  key={ind} cardInfo={product} isInCart={true} />
          })
        }
      </Container>
    )
}


export default CartWrapper;