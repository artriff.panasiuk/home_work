import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './componens/App';
import { BrowserRouter } from 'react-router-dom';
import store from './store/store';
import {Provider} from 'react-redux'


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <BrowserRouter>
  <Provider store={store}>
    <App />
  </Provider>
  </BrowserRouter>
);

// Підключіти бібліотеку Redux.

// Для цього необхідно доповнити проект, створений у попередньому домашньому завданні homework3.

// Технічні вимоги:
// Підключити бібліотеки redux, react-redux и redux-thunk.
// Після отримання масиву даних за допомогою AJAX запиту, записати їх у redux store.
// При виведенні компонентів на сторінку – дані брати з redux store.!!!!!!!!
// При відкритті будь-яких молальних вікон - інформація про те, чи відкрито зараз модальне вікно, повинна зберігатися в redux store.
// Всі action-и повинні бути виконані у вигляді функцій використовуючи функціонал redux-thunk.