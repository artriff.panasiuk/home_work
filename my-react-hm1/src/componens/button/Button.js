import { Btn } from './styles';
import { Component } from 'react';

class Button extends Component {

  render() {
    const {backgroundColor,text,onClick} =this.props;
    return (
      <Btn onClick={onClick} style={{ 
        backgroundColor: backgroundColor 
      }}>
        {text}
      </Btn>
    )
  };
}

export default Button;
