import styled from "styled-components";
export const Btn = styled.div`
min-width: 120px;
border-radius: 6px;
border: 1px solid transparent;
cursor:pointer;
padding: 10px 10px;
text-align: center;


&:hover {
  border-color: white;
  color: white;
}
`
