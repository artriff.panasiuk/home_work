import {Main} from './styles';
import Button from './button/Button.js';
import Modal from './modal/Modal.js';
import React from 'react';
class App extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      firstModalRender: false,
      secondModalRender: false
    }
    this.showFirstModal = this.showFirstModal.bind(this);
    this.showSecondModal = this.showSecondModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }
  showFirstModal(){
    this.setState({
      firstModalRender: true
    })
  }
  showSecondModal(){
    this.setState({
      secondModalRender: true
    })
  }
  closeModal(){
    this.setState({
      firstModalRender: false,
      secondModalRender: false
    })
  }
  render() {
    const buttonFirst = <Button backgroundColor="rgb(0 0 0 / 28%)" text="Oк"/>;
    const buttonSecond = <Button backgroundColor="rgb(0 0 0 / 28%)" text="Cancel"/>;
    const buttonThird = <Button backgroundColor="lightsalmon" text="More information"/>;
    let firsrModalElement = null;
    let secondModalElement = null;
    if (this.state.firstModalRender) {
      firsrModalElement =  <Modal header="Do you want to delete this file?" text="Once you delete this file, it won’t be possible to undo this action. are you sure you want to delete it?" closeButton={true} onClose={this.closeModal}>{buttonFirst}{buttonSecond}</Modal>;
    }
    if (this.state.secondModalRender) {
      secondModalElement =  <Modal header="React Components" text="Components are independent and reusable bits of code. They serve the same purpose as JavaScript functions, but work in isolation and return HTML.
      Components come in two types, Class components and Function components." closeButton={false} onClose={this.closeModal}>{buttonThird}</Modal>;
    }
    return (
    <Main>
    <Button onClick={this.showFirstModal} backgroundColor="mediumslateblue" text="Open first modal"/>
    <Button onClick={this.showSecondModal} backgroundColor="olive" text="Open second modal"/>
    {firsrModalElement}
    {secondModalElement}
    </Main>
    );
  };
}

export default App;
