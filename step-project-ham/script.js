"use strict";
document.addEventListener('DOMContentLoaded', function () {
  const menu = document.querySelector('.our-services__menu'),
    workMenu = document.querySelector('.our-work__menu'),
    content = document.querySelectorAll('.our-services__content'),
    contentWork = document.querySelectorAll('.our-work__item'),
    tabs = document.querySelectorAll('.our-services__tab'),
    workTabs = document.querySelectorAll('.our-work__tab'),
    btnWorkLoad = document.querySelector('.our-work-load'),
    btnGallery = document.querySelector('.btn-gallery'),
    grid = document.querySelector('.grid');

  //search input
    const btnSearch = document.querySelector('.header__nav-search');
    const sectionTitle = document.querySelectorAll('.section__title');
    let isInputExists = false;
    const createInput = () =>{
      btnSearch.insertAdjacentHTML("afterend",
        '<input class = "input-search">'
      )
    };
    const showSection = (value) =>{
      sectionTitle.forEach(title => {
        function scrollTo(hash) {
          location.hash = "#" + hash;
      }
        if (title.textContent.toLowerCase().indexOf(value.toLowerCase()) !== -1) {
          scrollTo(title.id)
        }
      })
    };
    btnSearch.addEventListener("click",() =>{
      if (isInputExists) {
        const inputSearch = document.querySelector('.input-search');
        if (inputSearch.value) {
          showSection(inputSearch.value);
        } else {
          inputSearch.placeholder = "Write name section!!";
        }
      } else {
        createInput();
        isInputExists = true;
      }
    });
    
  //tab
  function makeTabs(parentElement, tabElements, activeTabSelector, activeContentSelector, category, contentElements, tabAll = "") {
    parentElement.addEventListener('click', (e) => {
      tabElements.forEach(item => {
        item.classList.remove(activeTabSelector);
      })
      showContent(e.target);
    });

    function showContent(tab) {
      contentElements.forEach(item => {
        item.classList.remove(activeContentSelector);
        if (tab.dataset[category] == tabAll) {
          item.classList.add(activeContentSelector);
        }
        else if (item.dataset[category] == tab.dataset[category]) {
          item.classList.add(activeContentSelector);
        }
      })
      tab.classList.add(activeTabSelector);
    }
  };
  //tab our services
  makeTabs(menu, tabs, "active-tab", "visible-content", "services", content);
  //tab our work
  makeTabs(workMenu, workTabs, "our-work__tab-active", "visible-workContent", "ourWork", contentWork, "all");

  //load btn
  let flag = true;
  btnWorkLoad.addEventListener('click', () => {
    const showSpinner = () => {
      btnWorkLoad.insertAdjacentHTML("afterbegin",
      '<div class = "loader__wrapper"><div class = "loader"></div></div>'
    )
    };
    if (flag) {
      showSpinner();
      let wrapperHeight = window.getComputedStyle(btnWorkLoad.previousElementSibling, null).getPropertyValue("height").slice(0, -2);
      setTimeout(() => {
        btnWorkLoad.previousElementSibling.style.height = `${2 * wrapperHeight}px`;
        btnWorkLoad.firstChild.remove();
      },2000)
      flag = !flag;
    } else {
      showSpinner();
      setTimeout(() => {
        btnWorkLoad.previousElementSibling.style.height = `auto`;
        btnWorkLoad.firstChild.remove();
        btnWorkLoad.remove();
      },1000);
    }
  });
  btnGallery.addEventListener('click', () => {
    const showSpinner = () => {
      btnGallery.insertAdjacentHTML("afterbegin",
      '<div class = "loader__wrapper"><div class = "loader"></div></div>'
    )
    };
      showSpinner();
      setTimeout(() => {
        btnGallery.previousElementSibling.style.maxHeight = `fit-content`;
        btnGallery.previousElementSibling.style.overflow = `visible`;
        btnGallery.firstChild.remove();
        btnGallery.remove();
      },2000);
  });
  //Slider
  const clientBase = [
    { 
      name: "Maria",
      surname: "Red",
      photo: "img/feedback-avatar/feedback__avatar2.png",
      position: "Front-end Developer",
      quote: "If you're trying to create a company, it's like baking a cake. You have to have all the ingredients in the right proportion.",
      getFullname: function(){
        return this.name + " " + this.surname
      }
    },
    { 
      name: "Oleg",
      surname: "Procopenko",
      photo: "img/feedback-avatar/feedback__avatar3.png",
      position: "Back-end Developer",
      quote: "A company is only as good as the people it keeps.",
      getFullname: function(){
        return this.name + " " + this.surname;
      }
    },
    { 
      name: "Наsan",
      surname: "Ali",
      photo: "img/feedback-avatar/feedback__avatar1.png",
      position: "UX Designer",
      quote: "Money and success don’t change people; they merely amplify what is already there.",
      getFullname: function(){
        return this.name + " " + this.surname;
      }
    },
    { 
      name: "Olga",
      surname: "Krut",
      photo: "img/feedback-avatar/feedback__avatar4.png",
      position: "Team Lead",
      quote: "The whole secret of a successful life is to find out what is one’s destiny to do, and then do it.",
      getFullname: function(){
        return this.name + " " + this.surname;
      }
    },
    { 
      name: "George",
      surname: "Amster",
      photo: "img/feedback-avatar/contact__pic1.png",
      position: "HR Recuiter",
      quote: "Life is not a problem to be solved, but a reality to be experienced.",
      getFullname: function(){
        return this.name + " " + this.surname;
      }
    },
    { 
      name: "Anny",
      surname: "Root",
      photo: "img/feedback-avatar/contact__pic2.png",
      position: "QA engineer",
      quote: "Turn your wounds into wisdom",
      getFullname: function(){
        return this.name + " " + this.surname;
      }
    },
    { 
      name: "Jack",
      surname: "Stone",
      photo: "img/feedback-avatar/contact__pic3.png",
      position: "Fullstack developer",
      quote: "Every company's greatest assets are its customers, because without customers there is no company.",
      getFullname: function(){
        return this.name + " " + this.surname;
      }
    }
  ];

  //render small avatar in slider from client base
  const slidesContainer = document.querySelector('.slides');
  const renderSlides = (base) => {
    base.forEach(client =>{
      slidesContainer.insertAdjacentHTML('afterbegin', `
      <div class="small-avatar__wrapper">
      <img data-client="${client.getFullname()}" src="${client.photo}" alt="feedback__avatar" class="nav__avatar">
      </div>
      `)
    });
  };

  renderSlides(clientBase);
  
  const feedbackContent = document.querySelector('.feedback__slider');
  const changeContent = (client) => {
    feedbackContent.firstElementChild.remove();
    feedbackContent.insertAdjacentHTML('afterbegin',`
    <div class="feedback__content">
    <p class="feedback__text">${client.quote}</p>
  <p class="feedback__author title--bold-green">${client.getFullname()}</p>
  <p class="feedback__position">${client.position}</p>
  <div class="avatar__wrapper">
    <img src="${client.photo}" alt="feedback__avatar" class="feedback__avatar">
  </div>
  </div>
    `);
  };
  changeContent(clientBase[clientBase.length - 1]);
  const navMenu = document.querySelector('.feedback__nav');
  navMenu.addEventListener('click', (e) => {
    if (e.target.dataset.client) {
      clientBase.forEach(client => {
       if (client.getFullname() == e.target.dataset.client) {
        changeContent(client);
       }
      });
    }
  });

  //slides
  const btnNext = document.querySelector('.next-step-btn');
  const btnPrev = document.querySelector('.previous-step-btn');
  slidesContainer.style.width = `${clientBase.length*117}px`;
  btnPrev.disabled = true;
  let stepLimit = clientBase.length - 4;
  let step = 0;
  let count = 0;
  const removeSlide =  (diraction) =>{
    switch (diraction) {
      case "right":
        step += 117;
        count++;
        break;
        case "left":
        step -= 117;
        count++;
          break;
    }
    slidesContainer.style.transform = `translateX(-${step}px)`;
  };
  btnNext.addEventListener('click', ()=>{
    btnPrev.disabled = false;
    if(count < stepLimit){
      removeSlide("right");
      if(count == stepLimit) {
        btnNext.disabled = true;
        count = 0;
      }
    } 
  });
  btnPrev.addEventListener('click', ()=>{
    btnNext.disabled = false;
    if(count < stepLimit){
    removeSlide("left");
    if (count == stepLimit) {
      btnPrev.disabled = true;
      count = 0;
    }
    } 
  });

//  Masonry
  let msnry = new Masonry(grid, {
    itemSelector: '.grid-item',
    gutter: 20
  });

});
