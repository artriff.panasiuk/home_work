import Button from './../componens/button/Button.js'
import React from "react";
import { render, unmountComponentAtNode } from "react-dom";

let container = null;
beforeEach(() => {
  // подготавливаем DOM-элемент, куда будем рендерить
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // подчищаем после завершения
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("renders with a text", () => {
  render(<Button text="Button" />,container);

  expect(container.textContent).toBe("Button");
});

it("renders with a background", () => {

  render(<Button backgroundColor="black" />, container);

  expect(container.firstChild.style.backgroundColor).toBe("black");
});