import { CardContainer, CardHeader, ImageContainer, CardDesc, CardFooter } from './styles';
import Button from '../button/Button.jsx';
import CheckboxStar from '../checkboxStar/CheckboxStar.jsx';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faXmark } from '@fortawesome/free-solid-svg-icons';
const ProductCard = ({ addToFavoriteFunc, cardInfo, showModal, isFavoriteProd, isInCart, showDeleteModal }) => {

  const changeFavorites = () => {
    addToFavoriteFunc(cardInfo, !isFavoriteProd);
  }
  const { productName, price, imgURL, productArticle, productColor } = cardInfo;
  return (
    <CardContainer>
      <ImageContainer>
        {isInCart && <FontAwesomeIcon className='btn-close' icon={faXmark} onClick={() => showDeleteModal(cardInfo)} />}
        <img src={imgURL} alt="card img" />
      </ImageContainer>
      {!isInCart ? (
        <CardHeader onClick={changeFavorites}>
          <h3 className="card__title">{productName}</h3>
          <CheckboxStar isFavorite={isFavoriteProd} />
        </CardHeader>
      )
        : (
          <CardHeader>
            <h3 style={{ cursor: "auto" }} className="card__title">{productName}</h3>
          </CardHeader>
        )
      }
      <CardDesc>
        <p className="card__product-desk">Product color: {productColor}</p>
        <p className="card__product-desk">Article: {productArticle}</p>
      </CardDesc>
      <CardFooter>
        <p className='card__product-price'>{price} &#x24;</p>
        {isInCart ? <Button text="Buy" backgroundColor="chocolate" /> : <Button onClick={() => showModal(cardInfo)} text="add to cart" backgroundColor="rgb(215, 201, 184)" />}

      </CardFooter>
    </CardContainer>
  )
}

ProductCard.propTypes = {
  cardInfo: PropTypes.object,
  addToFavoriteFunc: PropTypes.func,
  addToCartFunc: PropTypes.func
};

ProductCard.defaultProps = {
  arrayCaddToFavoriteFuncart: null,
  addToCartFunc: null,
  isFavoriteProd: false,
  deleteCard: null,
  isInCart: false
};

export default ProductCard;