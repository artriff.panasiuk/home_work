import { Container } from './styles';
import ProductCard from '../card/ProductCard.jsx'
import PropTypes from 'prop-types';
const CartWrapper = ({ cardsArray, addToFavorite,showDeleteModal }) => {
    let cards = null;
    if (Array.isArray(cardsArray)) {
      cards = cardsArray.map((product, ind) => {
        return <ProductCard addToFavoriteFunc={addToFavorite}  key={ind} cardInfo={product} isInCart={true} showDeleteModal={showDeleteModal}/>
      });

    }
    return (
      <Container>
        {cards}
      </Container>
    )
}

CartWrapper.propTypes = {
  addToCart: PropTypes.func,
  addToFavorite: PropTypes.func
};

CartWrapper.defaultProps = {
  addToCart: null,
  addToFavorite: null,
  deleteCard: null
};

export default CartWrapper;