import { Container } from './styles';
import ProductCard from '../card/ProductCard.jsx'
import PropTypes from 'prop-types';
const CardWrapper = ({ cardsArray, showModal, addToFavorite }) => {
  let cards = null;
  let arrayFavorite = localStorage.getItem("FavoriteProd");
  if (Array.isArray(cardsArray)) {
    if (arrayFavorite) {
      arrayFavorite = JSON.parse(arrayFavorite);
      cards = cardsArray.map((product, ind) => {
        let sameElem = arrayFavorite.find(card => card.productArticle === product.productArticle);
        if (sameElem) {
          return <ProductCard addToFavoriteFunc={addToFavorite} showModal={showModal} key={ind} cardInfo={product} isFavoriteProd={true} />
        } else {
          return <ProductCard addToFavoriteFunc={addToFavorite} showModal={showModal} key={ind} cardInfo={product} />
        }
      });
    } else {
      cards = cardsArray.map((product, ind) => {
        return <ProductCard addToFavoriteFunc={addToFavorite} showModal={showModal} key={ind} cardInfo={product} />
      });
    }
  }
  return (
    <Container>
      {cards}
    </Container>
  )
}

CardWrapper.propTypes = {
  addToCart: PropTypes.func,
  addToFavorite: PropTypes.func
};

CardWrapper.defaultProps = {
  addToCart: null,
  addToFavorite: null
};

export default CardWrapper;