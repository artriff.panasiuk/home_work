import { HeaderSection, Container, Cart, Favorite } from './styled';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart } from '@fortawesome/free-solid-svg-icons';
import { faCartArrowDown } from '@fortawesome/free-solid-svg-icons';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';

const Header = ({ arrayFavorite, arrayCart }) => {

  return (
    <Container>
      <nav>
        <NavLink to="/"
          style={{ textDecoration: "none" }}>
          <HeaderSection>Guitar</HeaderSection>
        </NavLink>
        <div className='icons-container'>
          <NavLink to="/cart">
            <Cart>
              <FontAwesomeIcon icon={faCartArrowDown} />
              <span>{arrayCart.length}</span>
            </Cart>
          </NavLink>
          <NavLink to="/favorite">
            <Favorite>
              <FontAwesomeIcon icon={faHeart} />
              <span>{arrayFavorite.length}</span>
            </Favorite>
          </NavLink>
        </div>
      </nav>
    </Container>
  );
}

Header.propTypes = {
  arrayFavorite: PropTypes.array,
  arrayCart: PropTypes.array
};

Header.defaultProps = {
  arrayFavorite: [],
  arrayCart: []
};

export default Header;