import { Container } from './styles';
import ProductCard from '../card/ProductCard.jsx'
import PropTypes from 'prop-types';
const FavoriteCardWrapper = ({ cardsArray, showModal, addToFavorite }) => {
    let cards = null;
    if (Array.isArray(cardsArray)) {
      cards = cardsArray.map((product, ind) => {
        return <ProductCard addToFavoriteFunc={addToFavorite} showModal={showModal} key={ind} cardInfo={product} isFavoriteProd={true} />
      });

    }
    return (
      <Container>
        {cards}
      </Container>
    )
}

FavoriteCardWrapper.propTypes = {
  addToCart: PropTypes.func,
  addToFavorite: PropTypes.func
};

FavoriteCardWrapper.defaultProps = {
  addToCart: null,
  addToFavorite: null
};

export default FavoriteCardWrapper;